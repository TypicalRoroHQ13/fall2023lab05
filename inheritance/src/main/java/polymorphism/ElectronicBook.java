package polymorphism;

public class ElectronicBook extends Book {
    private int numberBytes;

    public ElectronicBook(String title, String author, int bytes){
        super(title, author);

        this.numberBytes=bytes;
    }

    public int getNumberBytes(){
        return this.numberBytes;
    }

    @Override
    public String toString(){
        String fromBase=super.toString();

        return fromBase + ", Bytes: " + this.numberBytes; 
    }
}
