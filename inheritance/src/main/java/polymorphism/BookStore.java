package polymorphism;

public class BookStore {
    public static void main(String[] args){
        Book[] bookSection=new Book[5];

        bookSection[0]=new Book("The Hunger Games", "Suzanne Collins");
        bookSection[1]=new ElectronicBook("The Hobbit", "J.R.R Tolkien", 30);
        bookSection[2]=new ElectronicBook("IT", "Stephen King", 100);
        bookSection[3]=new Book("Harry Potter", "J.K Rowling");
        bookSection[4]=new ElectronicBook("One Piece", "Eichiro Oda", 125);


        for(int i=0;i<bookSection.length;i++){
            System.out.println(bookSection[i]);
        }

        ElectronicBook b=(ElectronicBook)bookSection[0];

        System.out.println(b.getNumberBytes());
    }
}
